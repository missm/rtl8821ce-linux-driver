# rtl8821ce-linux-driver

我自己在安装RTL8821CE网卡时，在Linux Kernel 5.0+的内核中，会出现编译错误的问题，在网上搜到了解决方案，并照之执行后的可正常使用的版本。

### 安装

克隆仓库后，进入`src`目录，然后执行：

```shell
make
sudo make install
sudo modprobe -a 8821ce
```

然后重启机器即可。

### 参考资料

* [驱动源代码地址](https://github.com/endlessm/linux/tree/master/drivers/net/wireless/rtl8821ce)
* [驱动编译错误解决方案](https://www.cnblogs.com/imagineAct/p/11286072.html)